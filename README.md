# Test E2E

This project is a sample setup for E2E testing with cypress against a
a PHP application.

It is assumed that cypress is available locally. Otherwise
it must be added via:

``` bash
yarn add cypress start-server-and-test -D
```
