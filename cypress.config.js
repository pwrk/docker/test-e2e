const { defineConfig } = require('cypress')

module.exports = defineConfig({ 
  "e2e": {
    "baseUrl": "http://localhost:8000",
    "fixturesFolder": "test/cypress/fixtures",
    "specPattern": "test/cypress/integration",
    "screenshotsFolder": "test/cypress/screenshots",
    "supportFile": "test/cypress/support/index.js",
    "videosFolder": "test/cypress/videos",
    "video": false,
    "viewportHeight": 1280,
    "viewportWidth": 1480,
    "chromeWebSecurity": false,
    "excludeSpecPattern": [
      "*~"
    ]
  }
})
